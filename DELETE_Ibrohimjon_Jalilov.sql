-- 1. Delete rental records associated with the film
DELETE FROM public.rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM public.inventory
    WHERE film_id = (
        SELECT film_id
        FROM public.film
        WHERE title = 'Skyward Bound'
    )
);

-- 2. Delete the film from the inventory
DELETE FROM public.inventory
WHERE film_id = (
    SELECT film_id
    FROM public.film
    WHERE title = 'Skyward Bound'
);

-- 3. Finally, remove the film itself
DELETE FROM public.film
WHERE title = 'Skyward Bound';


-- Assuming your customer_id is known or retrieved based on my name
-- 1. Delete your payment records (assuming there's a direct link to customer_id)
DELETE FROM public.payment
WHERE customer_id = (
    SELECT customer_id
    FROM public.customer
    WHERE first_name = 'Ibrohim' AND last_name = 'Jalilov'
    LIMIT 1 -- In case there are multiple entries, ensures only your record is used
);

-- 2. Delete your rental records
DELETE FROM public.rental
WHERE customer_id = (
    SELECT customer_id
    FROM public.customer
    WHERE first_name = 'Ibrohim' AND last_name = 'Jalilov'
    LIMIT 1
);






